# Signals Mobile

**Signals.aia** is the App Inventor source code (http://appinventor.mit.edu/)

**Signals.apk** is the Android app built by App Inventor

**main.cpp** is the example Arduino code to drive a relay or other device from the app


The app makes sound signals for sailing races by sending commands to the USB serial port.

Two devices are supported currently:

(1) USB relay HW-667  
(2) Micro-controllers (eg Arduino Uno)  

For the Arduino, the device can be set in the app to "Generic One/Zero" so Arduino receives a "1" from the app when the signal should start and a "0" when it should stop. Code on the Arduino must exist to process the "1" and "0" characters sent. An example of this is included in file main.cpp.

The app can control any relay device connected, but the intention is to control an electric horn to make the sounds for a race.

If no device is connected, the app will play the sound of an air horn.

Ian Cherrill
ian.cherrill@rotor-rig.com
2021
