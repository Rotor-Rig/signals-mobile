#include <Arduino.h>

char incomingByte;

//the signal pin for the relay
//can be set to any available digital pin
//on the Arduino
const int RELAY_PIN = 3;

void setup() {
  Serial.begin(9600); //baud rate - do not change
  pinMode(RELAY_PIN,OUTPUT);
}

void loop() {
  if(Serial.available() > 0){
    incomingByte = Serial.read();
    if(incomingByte == '0'){
       digitalWrite(RELAY_PIN,LOW); //deactivate
    }
    if(incomingByte == '1'){
       digitalWrite(RELAY_PIN,HIGH); //activate
    }
  }
}
